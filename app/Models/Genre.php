<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Genre extends Model
{
	protected $table = "t_genres";
    protected $fillable = [
    	'type','status'
    ];
}