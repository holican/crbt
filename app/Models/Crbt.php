<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Crbt extends Model
{
	protected $table = "t_crbt";
    protected $fillable = [
    	'ring_id','genre_id','ring_label','ring_icon','ring_author','ring_fee','song_url','keyword','status'
    ];
}