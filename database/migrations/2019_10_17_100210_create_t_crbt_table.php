<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTCrbtTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('t_crbt', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('ring_id')->nullable()->default(null);
            $table->unsignedInteger('genre_id');
            $table->string('ring_label')->nullable()->default(null);
            $table->string('ring_icon')->nullable()->default(null);
            $table->string('ring_author')->nullable()->default(null);
            $table->unsignedInteger('ring_fee')->nullable()->default(null);
            $table->string('song_url')->nullable()->default(null);
            $table->string('keyword')->nullable()->default(null);
            $table->integer('status')->nullable()->default(null);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('t_crbt');
    }
}
