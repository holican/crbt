<?php

use Illuminate\Database\Seeder;
use App\Models\Genre;

class GenreTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
     public function run()
    {
        $genre1 = Genre::create([
        	'type' => 'Holiday',
        	'status' => '1'
        ]);
        $genre2 = Genre::create([
        	'type' => 'Electronic',
        	'status' => '1'
        ]);
        $genre3 = Genre::create([
        	'type' => 'Pop',
        	'status' => '1'
        ]);
        $genre4 = Genre::create([
        	'type' => 'Hip Hop',
        	'status' => '1'
        ]);
        $genre5 = Genre::create([
        	'type' => 'Children',
        	'status' => '1'
        ]);
        $genre6 = Genre::create([
        	'type' => 'Electronic',
        	'status' => '1'
        ]);
        $genre7 = Genre::create([
        	'type' => 'Lounge',
        	'status' => '1'
        ]);
        $genre8 = Genre::create([
        	'type' => 'Classical',
        	'status' => '1'
        ]);
        $genre9 = Genre::create([
        	'type' => 'Ambient',
        	'status' => '1'
        ]);
        $genre10 = Genre::create([
        	'type' => 'Latin',
        	'status' => '1'
        ]);
        $genre11 = Genre::create([
        	'type' => 'World',
        	'status' => '1'
        ]);
        $genre12 = Genre::create([
        	'type' => 'Reggae',
        	'status' => '1'
        ]);
        $genre13 = Genre::create([
        	'type' => 'Jazz',
        	'status' => '1'
        ]);
        $genre14 = Genre::create([
        	'type' => 'Country',
        	'status' => '1'
        ]);
        $genre15 = Genre::create([
        	'type' => 'Funk',
        	'status' => '1'
        ]);
        $genre16 = Genre::create([
        	'type' => 'Blues',
        	'status' => '1'
        ]);
        $genre17 = Genre::create([
        	'type' => 'Rock',
        	'status' => '1'
        ]);
        $genre18 = Genre::create([
        	'type' => 'Folk',
        	'status' => '1'
        ]);
        $genre19 = Genre::create([
        	'type' => 'Acoustic',
        	'status' => '1'
        ]);
        $genre20 = Genre::create([
        	'type' => 'Singer-Songwriter',
        	'status' => '1'
        ]);
        $genre21 = Genre::create([
        	'type' => 'Indie',
        	'status' => '1'
        ]);
        $genre22 = Genre::create([
        	'type' => 'Cinematic',
        	'status' => '1'
        ]);
        $genre23 = Genre::create([
        	'type' => 'Soul & RnB',
        	'status' => '1'
        ]);
    }
}
