<?php

use Illuminate\Database\Seeder;
use App\Models\Crbt;

class CrbtTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $crbt1 = Crbt::create([
        	'ring_id' => '0000007205',
			'genre_id' => '1',
			'ring_label' => 'Chit Thay Lar Ko Ver2',
			'ring_author' => 'Po Po Heather',
			'ring_fee' => '315',
			'song_url' => 'http://www.mptringtune.com.mm/colorring/wav/sys/895.wav',
			'keyword' => 'Chit Thay Lar Ko Ver2 Po Po Heather Contents Hub',
			'status' => '1'
        ]);
        $crbt2 = Crbt::create([
        	'ring_id' => '0100045202',
			'genre_id' => '2',
			'ring_label' => 'Kway Kar Pyat See Var3',
			'ring_author' => 'Htet Mon',
			'ring_fee' => '315',
			'song_url' => 'http://www.mptringtune.com.mm/colorring/wav/sys/896.wav',
			'keyword' => 'Kway Kar Pyat See Ver3 Htet Mon etrade',
			'status' => '1'
        ]);
        $crbt3 = Crbt::create([
        	'ring_id' => '0000471203',
			'genre_id' => '1',
			'ring_label' => 'Pyan Lar Khae Par V3',
			'ring_author' => 'Wine Su Khine Thein',
			'ring_fee' => '315',
			'song_url' => 'http://www.mptringtune.com.mm/colorring/wav/sys/671.wav',
			'keyword' => 'Pyan Lar Khae Par V3 Wine Su Khine Thein Unico Myanmar',
			'status' => '1'
        ]);
        $crbt4 = Crbt::create([
        	'ring_id' => '0000472203',
			'genre_id' => '2',
			'ring_label' => 'Sate Kuu Yin Eain Mat Kabar',
			'ring_author' => 'Ya Tha',
			'ring_fee' => '315',
			'song_url' => 'http://www.mptringtune.com.mm/colorring/wav/sys/672.wav',
			'keyword' => 'Sate Kuu Yin Eain Mat Kabar Ya Tha Unico Myanmar',
			'status' => '1'
        ]);
        $crbt5 = Crbt::create([
        	'ring_id' => '0000473203',
			'genre_id' => '2',
			'ring_label' => 'Sate Pu Tal',
			'ring_author' => 'Lay Phyu',
			'ring_fee' => '315',
			'song_url' => 'http://www.mptringtune.com.mm/colorring/wav/sys/673.wav',
			'keyword' => 'Sate Pu Tal Lay Phyu Unico Myanmar',
			'status' => '1'
        ]);
        $crbt6 = Crbt::create([
        	'ring_id' => '0000474203',
			'genre_id' => '3',
			'ring_label' => 'Shin Than Nay Mal',
			'ring_author' => 'Nyi Min Khine',
			'ring_fee' => '315',
			'song_url' => 'http://www.mptringtune.com.mm/colorring/wav/sys/674.wav',
			'keyword' => 'Shin Than Nay Mal Nyi Min Khine Unico Myanmar',
			'status' => '1'
        ]);
        $crbt7 = Crbt::create([
        	'ring_id' => '0000475203',
			'genre_id' => '3',
			'ring_label' => 'Ta Khar Phwint Htar Mal V2',
			'ring_author' => 'Shwe Htoo',
			'ring_fee' => '315',
			'song_url' => 'http://www.mptringtune.com.mm/colorring/wav/sys/675.wav',
			'keyword' => 'Ta Khar Phwint Htar Mal V2 Shwe Htoo Unico Myanmar',
			'status' => '1'
        ]);
        $crbt8 = Crbt::create([
        	'ring_id' => '0000475203',
			'genre_id' => '3',
			'ring_label' => 'Way Mal So Tar Thi Hlat Nae',
			'ring_author' => 'Po Po',
			'ring_fee' => '315',
			'song_url' => 'http://www.mptringtune.com.mm/colorring/wav/sys/676.wav',
			'keyword' => 'Way Mal So Tar Thi Hlat Nae Po Po Unico Myanmar',
			'status' => '1'
        ]);
    }
}
